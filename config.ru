require 'rack/rewrite'

use Rack::Static, 
  :urls => ["css", "js", "images"],
  :root => "public"

run lambda { |env|
  [
    200, 
    {
      'Content-Type'  => 'text/html', 
      'Cache-Control' => 'public, max-age=86400' 
    },
    File.open('public/index2.html', File::RDONLY)
  ]
}

use Rack::Rewrite do
  rewrite "/", "/index2.html"
end

run Rack::File.new("public")
